### Checklist

- [ ] The code has 100% test coverage.
- [ ] The documentation was properly created or updated (if applicable) following the correct guidelines and appropriate language.
- [ ] I branched out from the latest main and the V2 tag was properly rebased from main.
